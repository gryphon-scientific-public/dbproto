# -*- coding: utf-8 -*-
from setuptools import setup

def readme():
    with open('README.rst') as f:
        return f.read()

setup(name='dbproto',
    version='0.1',
    description='Database prototyping tools for simple databases',
    long_description=readme(),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6',
        'Topic :: Database',
        'Intended Audience :: Developers',
        'Programming Language :: SQL'
    ],
    url='https://gitlab.com/gryphon-scientific/dbproto',
    author='Gautham Venugopalan, Gryphon Scientific, LLC',
    author_email='gautham@gryphonscientific.com',
    license='MIT',
    packages=['dbproto','dbproto.bsve'],
    install_requires=[
            'psycopg2',
            'pyyaml',
            'pandas',
            'requests'
            ],
    dependency_links=['https://github.com/yaml/pyyaml#egg=python-s3-4.1'],
    zip_safe=False,
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    scripts=['bin/build_db']
)