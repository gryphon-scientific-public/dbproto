# -*- coding: utf-8 -*-
import requests
import yaml
import logging
import time
import random
import sys
import hmac
import json
import pandas
import dbproto
import os
import hashlib

try:
    dbproto.setup_logging()
    logger = logging.getLogger(__name__)
    log_status = True
except:
    log_status = False

#defaults
__DEFAULT_APICONFIG__ = os.getenv('BSVE_CONFIG_PATH','bsve.yml')
logger.debug('API Configuration location {}'.format(__DEFAULT_APICONFIG__))

#stores are the dbs, sources are the propared endpoints for querying by apps
__datasource_url__ = 'https://api.bsvecosystem.net/data/v2/sources/'
__datastore_url__ = 'https://api.bsvecosystem.net/data/v2/stores/'

class AuthHeader(object):
    """
    Create authentication header as single value dict. Needs yml file like
    bsve.yml. If it fails to load the parameters throw an exception
    """
    def __init__(self,username='',apikey='',secretkey=''):
        
        self.username = username
        self.apikey = apikey
        self.key = ':'.join([apikey,secretkey])
        self.key = bytes(self.key.encode('latin-1'))
        if log_status:
            logger.debug('AuthHeader object created')
    
    def getHdr(self):
        timestamp = str(time.time())
        nonce = str(random.SystemRandom().randint(0,sys.maxsize))
        msg = ''.join([self.apikey,timestamp,nonce,self.username])
        msg = bytes(msg.encode('latin-1'))
        try:
            h = hmac.new(key=self.key,msg=msg,digestmod='sha1')
        except:
            
            h = hmac.new(key=self.key,msg=msg,digestmod=hashlib.sha1)
        sig = h.hexdigest()
        hdr = {'harbinger-authentication' : 'apikey={0};timestamp={1};nonce={2};signature={3}'.format(
            self.apikey,timestamp,nonce,sig)}
        if log_status:
            logger.debug('Header created')
        return hdr
    
def create_hdr(path=__DEFAULT_APICONFIG__):
    """
    Create a header object from a yaml file or environment keys. Yaml file overrides environment keys.
    Env keys (if used): 'BSVE_USERNAME', 'BSVE_APIKEY',and 'BSVE_SECRETKEY' respectively
    """
    username = os.getenv('BSVE_USERNAME', '')
    apikey = os.getenv('BSVE_APIKEY', '')
    secretkey = os.getenv('BSVE_SECRETKEY', '')

    try:
        with open(path,'rt') as f:
            params = yaml.safe_load(f.read())
    except:
        params=None
        if log_status:
            logger.warning('Cannot read params from {0}. Should create a new AuthHeader object'.format(path))
    if params:
        username = params.get('username',username)
        apikey = params.get('apikey',apikey)
        secretkey = params.get('secretkey',secretkey)

    return AuthHeader(username=username,apikey=apikey,secretkey=secretkey)

#set default header object
__hdr__ = create_hdr()

class BsveParams(dbproto.ConnParams):
    """
    Class to get bsve parameters, subclass of ConnParams object
    """
    def __init__(self,dct=dict(),hdr=__hdr__):
        """
        Return database connection parameters given a database name. 
        Rename the parameters to a ConnParams object.
        """
        self.resetParams()
        self.ds_id = None
        self.dbname = None
        self.desc = None
        self.hdr = hdr
        for key,val in dct.items():
            if hasattr(self,key):
                setattr(self,key,val)
            else:
                if log_status:
                    logger.warning('Attribute {0} does not exist in BsveParams, ignoring this value'.format(key))
        if (self.dbname is None and self.ds_id is None) or (self.dbname is not None and self.ds_id is not None):
            raise Exception('Must specify exactly one of dbname or ds_id')
        if self.dbname is None:
            self.dbname = get_datastore_name(self.ds_id)
        elif self.ds_id is None:
            self.ds_id = get_datastore_id(self.dbname)
        
        if self.ds_id is None or self.dbname is None:
            if log_status:
                logger.warning('Could not read the datastore id and/or the dbname')
        self.update_params()
        
    def update_params(self):
        """
        Update the database connection parameters from the API
        """
        params = None
        #params dict with format bsve_name dbproto_name which maps to psycopg2
        params_translator = {'db.host':'host',
                             'db.database':'database',
                             'db.username':'user',
                             'db.password':'password',
                             'db.port':'port'
                             }
        try:
            result = get(url=__datastore_url__+self.ds_id,fmt='dict',hdr=self.hdr)
            params = result['configuration']
            #if log_status:
                #logger.debug(params)
            for old_name,new_name in params_translator.items():
                params[new_name]=params.pop(old_name)
            self.host = params.get('host',None)
            self.database = params.get('database',None)
            self.user = params.get('user',None)
            self.port = params.get('port',None)
            self.password = params.get('password',None)
            if log_status:
                logger.debug('BsveParams object created')
            #if log_status:
                #logger.debug(self.getDict())
        except Exception as e:
            self.host=None
            self.database=None
            self.user=None
            self.port=None
            self.password=None
            if log_status:
                logger.warning('Could not find database, parameters set to None')
                logger.error(e,exc_info=True)

def validate_response(r):
    """
    Validate the response for success and status. Throw the errors for logging.
    """
    if log_status:
        logger.info('Query status: {0} - {1}'.format(r.status_code,r.json()['message']))
    r.raise_for_status()
    try:
        err_list = r.json()['errors']
    except:
        err_list = None
    if err_list:
        for i in err_list:
            if log_status:
                logger.error(i['errorMessage'])
        return False
    else:
        return True
    
def get(url,payload=None,fmt='response',hdr=__hdr__):
    """
    Build a query and return only the result section from the get request.
    Default format of response returns the requests.get object.
    """
    if log_status:
        logger.info('GET request to {0}'.format(url))
    fmts = ['json','df','dict','dict_raw','response']
    if fmt not in fmts:
        fmt='response'
        if log_status:
            logger.warning('Unrecognized format. Returning unmodified response object.')
    try:
        r = requests.get(url=url,json=payload,headers=hdr.getHdr())
        if not validate_response(r):
            raise Exception (r.json()['message'])
        #if log_status:
            #logger.debug(r.json()['result'])
        result_tmp = r.json()['result']
        if fmt=='json':
            #return results as single json item
            result = json.dumps(result_tmp)
        elif fmt=='df':
            #return a pandas dataframe
            result = pandas.DataFrame(result_tmp)
        elif fmt=='dict':
            #return a list of dict items
            result = result_tmp
        elif fmt=='dict_raw':
            #return raw dict without extracting results section
            result = r.json()
        elif fmt=='response':
            #return the unmodified response object
            result = r
        else:
            if log_status:
                logger.warning('Unrecognized format. Returning unmodified response object.')
            result = r
        if log_status:
            logger.debug('Returning result in format {0}'.format(fmt))
        return result
    except Exception as e:
        if log_status:
            logger.error(e,exc_info=True)
        return None

def create_env(env_params):
    """
    Create the datastore environment. Analagous function to dbproto.create_env
    """
    #check if the database exists. Delete it if it does. Then create it.
    exec_status = None
    try:
        if env_params.database.database is not None:
            delete_result = delete_datastore(ds_id=env_params.database.ds_id)
            if not delete_result:
                if log_status:
                    logger.warning('Failed to delete datastore {0}'.format(env_params.database.ds_id))
        ds_id = create_datastore(name=env_params.database.dbname,description=env_params.database.desc)
        env_params.database.ds_id = ds_id
        env_params.database.update_params()
    
        #install postgis if it's not already installed and if it should be.
        if env_params.postgis:
            postgis_result = install_extensions(env_params.database.ds_id,'postgis',env_params.database.hdr)
            if not postgis_result:
                if log_status:
                    logger.warning('Failed to create postgis')
        result = dbproto.execute_sql_fromfile(env_params.database,env_params.schemas)

        exec_status = True
    except Exception as e:
        if log_status:
            logger.error(e,exc_info=True)
        exec_status = False
    return exec_status
    
    
def create_datastore(name,description,ds_type='RDBMS',provider='PostgreSQL',hdr=__hdr__):
    """
    Create a datastore in the BSVE. Return the datastore id if successful. Else
    return None
    """
    payload = {'name': name, 'description': description,'type': ds_type, 'provider': provider}
    
    ds_id = None
    
    try:
        r = requests.post(url=__datastore_url__,json=payload,headers=hdr.getHdr())
        if validate_response(r):
            if log_status:
                logger.info('Datastore created: {0}'.format(name))
        else:
            raise Exception(r.json()['message'])
        ds_id = r.json()['result']['id']
    except Exception as e:
        if log_status:
            logger.error(e,exc_info=True)
    return ds_id

def delete_datastore(ds_id=None,name=None,hdr=__hdr__):
    """
    Delete the datastore given either the id or the name. Return None for
    failure to try, False for failure to delete (but tried), and True for 
    successful deletion
    """
    status = None
    if ds_id and name:
        raise Exception('Provide either id or name, not both')
    if name: #set the datastore id if only provided name
        ds_id = get_datastore_id(name)
    else: #if id was provided instead of name set display name to the id
        name = get_datastore_name(ds_id)
        
    if ds_id:
        try:
            r = requests.delete(url=__datastore_url__+ds_id,headers=hdr.getHdr())
            if validate_response(r):
                if log_status:
                    logger.info('Datastore deleted: {0}'.format(name))
                status = True
            else:
                status = False
                raise Exception (r.json()['message'])
        except Exception as e:
            if log_status:
                logger.error(e,exc_info=True)
    else:
        logging.error('datastore not found')
    
    return status
    
def get_datastore_id(name):
    """
    Return datastore id if it exists. Otherwise return none. Theoretically
    could return multiple ids if there are multiple things with the same name
    but the server should prevent two things with the same name from being
    created
    """
    ds_id = None
    try:
        df = get(url=__datastore_url__,fmt='df')
        
        if len(df)>0:
            ds_id = df.loc[df['name']==name]['id'].tolist()
            n_ids = len(ds_id)
            if n_ids==1:
                ds_id=ds_id[0]
            elif n_ids==0:
                ds_id = None
                if log_status:
                    logger.warning('No datastore identified with that name')
            else:
                if log_status:
                    logger.warning('More than one datastore identified with that name')
    except Exception as e:
        if log_status:
            logger.error(e,exc_info=True)
    if ds_id:
        if log_status:
            logger.info('Datastore ID obtained')
    return ds_id

def get_datastore_name(ds_id):
    """
    Return datastore name if it exists. Otherwise return none. Cannot return
    multiple names.
    """
    name = None
    try:
        result = get(url=__datastore_url__+ds_id,fmt='dict')
        name = result['name']
        if log_status:
            logger.info('Datastore name obtained')
    except Exception as e:
        if log_status:
            logger.error(e,exc_info=True)
    
    return name

def get_connection_params(ds_id):
    """
    Return database conneciton parameters given an ID. Rename the parameters
    to match dbproto names.
    """
    params = None
    #params dict with format bsve_name dbproto_name which maps to psycopg2
    params_translator = {'db.host':'host',
                         'db.database':'database',
                         'db.username':'user',
                         'db.password':'password',
                         'db.port':'port'
                         }
    try:
        result = get(url=__datastore_url__+ds_id,fmt='dict')
        params = result['configuration']
        for old_name,new_name in params_translator.items():
            params[new_name]=params.pop(old_name)
    except Exception as e:
        if log_status:
            logger.error(e,exc_info=True)
    
    return params

def get_extensions(ds_id,fmt='dict'):
    """
    Return the information about installed extensions from the API
    """
    result = None
    try:
        result = get(url=__datastore_url__+ds_id+'/extensions',fmt=fmt)
    except Exception as e:
        if log_status:
            logger.error(e,exc_info=True)
    return result

def install_extensions(ds_id,ext_names,hdr=__hdr__):
    """
    Install the requested postgres extension(s). ext_names is a list or str
    Unless they already exist, in which case then don't reinstall them.
    Returns None if nothing was installed.
    """
    installed_list = []
    chk = type(ext_names)
    if chk==str:
        ext_names=[ext_names]
    elif chk==list:
        None
    else:
        raise Exception('Invalid type')
    
    ext_info = get_extensions(ds_id=ds_id,fmt='dict')
    ext_install = []
    for ext_name in ext_names:
        ext_exists = next((True for item in ext_info if item['name']==ext_name),False)
        if ext_exists:
            logging.info('Extension exists: {0}'.format(ext_name)) 
        else:
            logging.debug('Try to install/upgrade {0}'.format(ext_name))
            ext_install.append(ext_name)
    if len(ext_install)>0:
        url = __datastore_url__+ds_id+'/extensions'
        for ext_name in ext_install:
            r = requests.post(url=url,json=ext_name,headers=hdr.getHdr())
            if validate_response(r):
                if log_status:
                    logger.info('Extension installed: {0}'.format(ext_name))
                installed_list.append(ext_name)
            else:
                if log_status:
                    logger.error('Failed to install {0}'.format(ext_name))
    if len(installed_list)==0:
        installed_list = None
    return installed_list

def uninstall_extensions(ds_id,ext_names,hdr=__hdr__):
    """
    Uninstall the requested postgres extension(s). ext_names is a list or str
    Unless they already exists then don't install them. Returns None if nothing
    was installed.
    """
    uninstalled_list = []
    chk = type(ext_names)
    if chk==str:
        ext_names=[ext_names]
    elif chk==list:
        None
    else:
        raise Exception('Invalid type')
    
    ext_info = get_extensions(ds_id=ds_id,fmt='dict')
    ext_install = []
    for ext_name in ext_names:
        ext_exists = next((True for item in ext_info if item['name']==ext_name),False)
        if ext_exists:
            logging.debug('Try to uninstall: {0}'.format(ext_name)) 
            ext_install.append(ext_name)
        else:
            logging.info('Extension not installed: {0}'.format(ext_name))
    if len(ext_install)>0:
        url = __datastore_url__+ds_id+'/extensions'
        for ext_name in ext_install:
            del_url = url + '/' + ext_name
            r = requests.delete(url=del_url,json=ext_name,headers=hdr.getHdr())
            if validate_response(r):
                if log_status:
                    logger.info('Extension uninstalled: {0}'.format(ext_name))
                uninstalled_list.append(ext_name)
            else:
                if log_status:
                    logger.error('Failed to uninstall {0}'.format(ext_name))
    if len(uninstalled_list)==0:
        uninstalled_list = None
    return uninstalled_list    

def get_datastores(fmt='df'):
    """
    Return list of data stores that you have access to
    """
    data_stores = None
    try:
        data_stores = get(url=__datastore_url__,fmt=fmt,hdr=__hdr__)
    finally:
        return data_stores
    
if __name__ == '__main__':
    
    """
    EXAMPLES (uncomment the # lines to execute)
    --------------
    """
    
    """
    get a list of the accessible data stores (i.e. SQL/NoSQL tables)
    might be an empty result if you don't have access to any stores
    but it should return with status 200.
    """
    #data_stores = get(url=__datastore_url__,fmt='df')
    
    """
    get a list of the accessible data sources (i.e. prepared endpoints)
    shouldn't be an empty result
    """
    #data_sources = get(url=__datasource_url__,fmt='df')
    
    """
    Hard coded version of database store creation
    """
#    payload = {
#    "name": "Gryphon_Test",
#    "description": "Test 1 Postgre Data store",
#    "type": "RDBMS",
#    "provider": "PostgreSQL"
#    }
    #r = requests.post(url=__datastore_url__,json=payload,headers=__hdr__)
    
   
    """
    Function version of database store creation. Returns the id
    """
    #db_id = create_datastore(name='Gryphon_Test2',description='Test 1 Postgre Data store')

    """
    Get data store ID given name
    """
    #ds_id = get_datastore_id('Gryphon_Test')

    """
    Delete a data store given the name or the id
    """
#    ds_id = create_datastore(name='Gryphon_Test',description='Test 1 Postgres Data store')
#    r = delete_datastore(name='Gryphon_Test')
#    ds_id = create_datastore(name='Gryphon_Test2',description='Test 2 Postgres Data store')
#    r = delete_datastore(ds_id=ds_id)
    
    """
    Get parameters for a data store connection
    #DEPRECATED USE CONNECTION OBJECT NOW
    #
    """
#    try:
#        ds_id = get_datastore_id('Gryphon_Test')
#    except:
#        ds_id = create_datastore(name='Gryphon_Test',description='Test 1 Postgres Data store')
#    params = get_connection_params(ds_id)
    
    """
    Get BsveParams object from data store connection
    """
#    try:
#        conn_obj = BsveParams('Gryphon_Test')
#    except:
#        ds_id = create_datastore(name='Gryphon_Test',description='Test 1 Postgres Data store')
#        conn_obj = BsveParams('Gryphon_Test')
    
    