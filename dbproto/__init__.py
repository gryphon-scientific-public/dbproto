# -*- coding: utf-8 -*-
import psycopg2
import string
import re
import os
import logging
import logging.config
import csv
import sys
import yaml
import pandas as pd
from io import open
__ext_types__ = ('.csv',)

def setup_logging(
    default_path='logging.yml',
    default_level=logging.INFO,
    env_key='LOG_CFG'
):
    """
    Set up logging function configuration. Can use yml, defaults, or 
    environment key. 
    Function based on python's official logging setup tutorial.
    """
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)
try:
    setup_logging()
    logger = logging.getLogger(__name__)
    log_status = True
except:
    log_status = False

#Defaults for ConnParams
__DEFAULT_HOST__='localhost'
__DEFAULT_DATABASE__='postgres'
__DEFAULT_USER__ = 'postgres'
__DEFAULT_PASSWORD__ = None
__DEFAULT_PORT__ = 5432

#Defaults for EnvParams
__DEFAULT_SERVER__ = 'default'
__DEFAULT_ENVDB__ = 'test'
__DEFAULT_CONNECT__ = None
__DEFAULT_SCHEMAS__ = 'init_schemas.sql'
__DEFAULT_POSTGIS__ = False
__DEFAULT_WARNDROP__ = False
__DEFAULT_INITFOLDER__ = ''

#add to this if adding more params in the future
__params_list__ = ('servers','environments') 
__servers__ = dict(default=dict(cls='dbproto.DefaultParams',
                                host=__DEFAULT_HOST__,
                                database=__DEFAULT_DATABASE__,
                                user=__DEFAULT_USER__,
                                password=__DEFAULT_PASSWORD__,
                                port=__DEFAULT_PORT__))
__environments__ = dict(default=dict(server=__DEFAULT_SERVER__,
                                     database=__DEFAULT_ENVDB__,
                                     default_connect=__DEFAULT_CONNECT__,
                                     schemas=__DEFAULT_SCHEMAS__,
                                     postgis=__DEFAULT_POSTGIS__,
                                     warn_drop=__DEFAULT_WARNDROP__,
                                     init_folder=__DEFAULT_INITFOLDER__))
def _resolve(name):
    """
    Resolve a dotted name to a global object.
    This function borrowed from python logging module which is
    Copyright 2001-2014 by Vinay Sajip. All Rights Reserved.
    
    Logging module permissions:
    # Permission to use, copy, modify, and distribute this software and its
    # documentation for any purpose and without fee is hereby granted,
    # provided that the above copyright notice appear in all copies and that
    # both that copyright notice and this permission notice appear in
    # supporting documentation, and that the name of Vinay Sajip
    # not be used in advertising or publicity pertaining to distribution
    # of the software without specific, written prior permission.
    # VINAY SAJIP DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
    # ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
    # VINAY SAJIP BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
    # ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
    # IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
    # OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
    
    The rest of this module library is copywritten as noted in the repository.
    """
    name = name.split('.')
    used = name.pop(0)
    found = __import__(used)
    for n in name:
        used = used + '.' + n
        try:
            found = getattr(found, n)
        except AttributeError:
            __import__(used)
            found = getattr(found, n)
    return found   

class ConnParams(object):
    """Database connection parameters in a class."""
    def __init__(self, host=__servers__['default']['host'],
                 database=__servers__['default']['database'],
                 user=__servers__['default']['user'],
                 password=__servers__['default']['password'],
                 port=__servers__['default']['port']):
        """
        Set db connection parameters
        """
        self.host = host
        self.database = database.lower()
        self.user = user
        self.password = password
        self.port = port
    
    def getDict(self):
        """
        Return the parameters from host, database, user, and password in a dict 
        for passing to psycopg2 using the format:
            
        conn = psycopg2.connect(**ConnParams.returnDict())
        """
        return {'host':self.host, 'database':self.database, 'user':self.user,'password':self.password,'port':self.port}  
    
    def resetParams(self):
        """
        Reset the databse parameters to the default values. Note: Defaults may 
        have been overwritten by a config read operation.
        """
        params = __servers__['default']
        self.host = params['host']
        self.database = params['database'].lower()
        self.user = params['user']
        self.password = params['password']
        self.port = params['port']
        
class DefaultParams(ConnParams):
    """
    Database connection parameters set to default. Trivial subclass included
    for ease of compatibility
    """
    def __init__(self,dct=None):
        self.resetParams()
    

class SpecifyParams(ConnParams):
    """
    Database connection parameters when specified. Input parameters as a
    dict
    """
    def __init__(self,dct=dict()):
        self.resetParams()
        for key,val in dct.items():
            if hasattr(self,key):
                setattr(self,key,val)
            else:
                if log_status:
                    logger.warning('Attribute {0} does not exist in ConnParams, ignoring this value'.format(key))

class EnvParams(object):
    """
    Class containing dbproto environment parameters for different usage
    environments
    """
    def __init__(self,
                 server=__DEFAULT_SERVER__,database=__DEFAULT_ENVDB__,
                 default_connect=__DEFAULT_CONNECT__,
                 schemas=__DEFAULT_SCHEMAS__,postgis=__DEFAULT_POSTGIS__,
                 warn_drop=__DEFAULT_WARNDROP__,
                 init_folder=__DEFAULT_INITFOLDER__):
        """
        Database is set to test by default because some of dbproto won't work on postgres table
        default_connect is None by default because sometimes one is not needed.
        """
        if log_status:
            logger.debug(schemas)
        if os.path.isfile(schemas):
            self.schemas = schemas
        else:
            self.schemas = None
            if log_status:
                logger.warning('Schemas file not found. Defaulting to none.')
        if isinstance(postgis,(bool,)):
            self.postgis = postgis
        else:
            if log_status:
                logger.warning('Postgis variable must be boolean. Defaulting to false.')
            self.postgis=False
        if isinstance(warn_drop,(bool,)):
            self.warn_drop = warn_drop
        else:
            if log_status:
                logger.warning('warn_drop variable must be boolean. Defaulting to false.')
            self.warn_drop = False
        self.init_folder = init_folder
        
        #create ConnParams objects
        self.database = None
        self.default_connect = None
        
        try:
            self.database = get_server(server=server,database=database)
            if default_connect is not None:
                self.default_connect = get_server(server=server,database=default_connect)
        except Exception as e:
            if log_status:
                logger.error(e,exc_info=True)
            
def yml_config(filename='dbproto.yml'):
    """
    Read configuration from a yaml file. Return True if successful, False
    if not, and hopefully never return None.
    """
    global __params_list__
    global __servers__
    global __environments__
    exec_status = None
    servers_tmp = dict(__servers__)
    environments_tmp = dict(__environments__)
    
    try:
        with open(filename, 'rt') as f:
            params = yaml.safe_load(f.read())
            v = params.pop('version')
        if v == 1:
            if log_status:
                logger.debug('read yml params')
        else:
            raise Exception('Version number invalid or not provided.')
        
        #Throw a warning if the keys are not all relevant.
        for x in params.keys():
            if x not in __params_list__:
                if log_status:
                    logger.warning('Ignoring unknown config file parameter {0}'.format(x))
        
        #read environments to dict
        try:
            p = params['environments']
        except:
            p = dict()
            if log_status:
                logger.info('No environments defined in {0}'.format(filename))
        keys = __environments__.keys()
        for key,val in p.items():
            if key in keys:
                if log_status:
                    logger.warning('Overwriting environments param: {0}'.format(key))
            __environments__[key] = val
        
        #read servers to dict
        try:
            p = (params['servers'])
        except:
            p = dict()
            if log_status:
                logger.info('No servers defined in {0}'.format(filename))
        keys = __servers__.keys()
        for key,val in p.items():
            if key in keys:
                if log_status:
                    logger.warning('Overwriting servers param: {0}'.format(key))
            __servers__[key] = val
        
        exec_status = True
        if log_status:
            logger.info('yml params read complete')
    except Exception as e:
        if log_status:
            logger.error(e,exc_info=True)
            logger.warning('Encountered errors while reading parameters, undoing any partial changes to parameters.')
        __servers__ = dict(servers_tmp)
        __environments__ = dict(environments_tmp)
        exec_status = False
    finally:
        return exec_status

def get_env(env='default'):
    """
    Return an EnvParams type variable
    """
    global __environments__
    
    try:
        params = __environments__[env]
        #if log_status:
            #logger.debug(params)
    except:
        raise Exception('Could not get parameters for environment {0}'.format(env))
    return EnvParams(**params)

def get_server(server='default',database=None):
    """
    Return server parameters of some subclass of ConnParams
    """
    global __servers__
    try:
        params = dict(__servers__[server])
        #if log_status:
            #logger.debug(params)
    except:
        raise Exception('Could not get parameters for server {0}'.format(server))
    cls = params.pop('cls')
    
    try:
        cls = eval(cls,vars(dbproto))
    except (AttributeError, NameError):
        cls = _resolve(cls)
    if not issubclass(cls,ConnParams):
        raise Exception('Could not find class')
    x = cls(dct=params)
    if database is not None:
        x.database = database
    return x

def create_env(params=EnvParams(default_connect='postgres')):
    """
    Create the database. WARNING: This function will drop any
    existing database that shares its name with the one named in the class!
    """

    conn = None
    exec_status = None
    
    try:
        # try to connect to the db
        if not is_valid(params.default_connect.database):
            raise Exception('Invalid default connection name {0}'.format(params.default_connect.database))
        if not is_valid(params.database.database):
            raise Exception('Invalid database name {0}'.format(params.database.database))
        conn = psycopg2.connect(**params.default_connect.getDict())
        if log_status:
            logger.info('Connected to {0}'.format(params.default_connect.database))

        # get the params for the database to create
        if params.warn_drop:
            x = input('WARNING: this function drops a database named {0} from {1}. It is susceptible to injection (check validity of {0}). This function is for testing and NOT production. Type \'{0}\' to continue (without quotes): '.format(params.database.database,params.database.host))
            if x != params.database.database:
                raise Exception('Aborting per user instruction.')

        #check if the db that you want to create already exists
        cur = conn.cursor()
        sql = """SELECT EXISTS(
                 SELECT 1
                 FROM pg_database
                 WHERE datistemplate = false
                 AND datname = %s);"""
        cur.execute(sql,(params.database.database,))
        conn.commit()
        db_exists = cur.fetchone()[0]

        # if the db_exists drop it
        old_autocommit = bool(conn.autocommit)
        conn.autocommit = True

        if db_exists:
            dangerous_sql = """DROP DATABASE {0};""".format(params.database.database)
            cur.execute(dangerous_sql)
            if log_status:
                logger.info('Dropped existing database {0}'.format(params.database.database))

        # create the db
        dangerous_sql = """CREATE DATABASE {0} WITH ENCODING = 'UTF8'""".format(params.database.database)
        cur.execute(dangerous_sql)
        if log_status:
            logger.info('Created database {0}'.format(params.database.database))
        conn.autocommit = bool(old_autocommit)
        conn.close()

        # connect to the new database
        conn = psycopg2.connect(**params.database.getDict())
        if log_status:
            logger.info('Connected to {0}'.format(params.database.database))

        #create postgis and confirm
        if params.postgis:
            postgis_ver = create_postgis(conn)
            if postgis_ver == None:
                raise Exception('postgis install failed')
        
        #disconnect(conn)
        
        #build schemas
        #exec_status = execute_sql_fromfile(conn_params=params.database,sql_path=params.schemas)
        exec_status = execute_sql_fromfile(conn_params=conn,sql_path=params.schemas)
        if exec_status:
            if log_status:
                logger.info('Created schemas and tables')
        else:
            raise Exception('Failed to create')
        exec_status = True
    except Exception as e:
        if log_status:
            logger.error(e, exc_info=True)
            logger.debug(params.__dict__)
        exec_status = False
    finally:
        disconnect(conn)
        return exec_status


def init(params=EnvParams()):
    """
    Initialize the database in the order of table creation provided
    Pass in a variable of type EnvParams to include the connection parameters
    Doesn't matter if default_connect is specified or not.
    """
    conn = None
    exec_status = None
        
    try:
        #get list of tables created directly from the db. ignore special ones.
        if not is_valid(params.database.database):
            raise Exception('Invalid database name {0}'.format(params.database.database))
        conn = psycopg2.connect(**params.database.getDict())
        if log_status:
            logger.info('Connected to {0}'.format(params.database.database))

        cur = conn.cursor()
        sql = """SELECT table_schema, table_name FROM information_schema.tables 
                 WHERE table_schema not like 'pg_%' 
                 AND table_schema!='information_schema'
                 AND table_type='BASE TABLE'
                 AND table_name!='spatial_ref_sys';"""
        cur.execute(sql)
        db_immutable=cur.fetchall()
        cur.close()
        db_tables=[]
        for i in db_immutable: db_tables.append(i[0] + '.' + i[1])
        
        #get list of tables that were listed in the create statement
        script_tables = get_created_tables(params.schemas)
        
        #insert tables in the order the schemas were created
        n_added = 0
        if set(db_tables)>=set(script_tables):
            for x in script_tables:
                for file_ext in __ext_types__:
                    x_fmt = '{0}{1}'.format(re.sub(r'\.','_',x),file_ext)
                    x_fmt = os.path.join(params.init_folder,x_fmt)
                    if log_status:
                        logger.debug(x_fmt)
                    n_added += insert_csv(x_fmt,conn)
        else:
            raise Exception('Tables in create script not found in database.')

        if log_status:
            logger.info('Insert complete. Added {0} entries to database'.format(
                n_added))
        if n_added>0:
            vacuum_analyze(conn)
        else:
            if log_status:
                logger.warning('VACUUM ANALYZE was not run because no data added')
        exec_status = True
    except Exception as e:
        if log_status:
            logger.error(e, exc_info=True)
        exec_status = False
    finally:
        disconnect(conn)
        return exec_status

def vacuum_analyze(conn):
    """
    Run VACUUM ANALYZE on the whole postgres database
    """
    exec_status = None
    old_autocommit = bool(conn.autocommit)
    try:
        conn.autocommit = True
        sql = "VACUUM ANALYZE;"
        cur = conn.cursor()
        cur.execute(sql)
        cur.close()
        exec_status = True
        if log_status:
            logger.info('VACUUM ANALYZE complete')
    except:
        exec_status = False
        if log_status:
            logger.info('Error running VACCUM ANALYZE')
    finally:
        conn.autocommit = bool(old_autocommit)
        return exec_status


def execute_sql_fromfile(conn_params,sql_path):
    """
    Execute sql from a .sql file. conn_params can be a psycopg2 connection 
    object or a ConnParams object
    
    Warning: This is for initialization. Do not use this with user inputs.
    """
    conn = None
    exec_status = None
    disconnect_flag = False
    try:
        if issubclass(conn_params.__class__,ConnParams):
            conn = psycopg2.connect(**conn_params.getDict())
            disconnect_flag = True
        elif issubclass(conn_params.__class__,psycopg2.extensions.connection):
            conn = conn_params
            disconnect_flag = False
        else:
            raise Exception('conn_params must be a ConnParams object or a psycopg2 connection')
        cur = conn.cursor()
        with open(sql_path,'r',encoding='utf-8') as s:
            sql_script = s.read()
            cur.execute(sql_script)
        conn.commit()
        cur.close()
        exec_status = True
    except Exception as e:
        if log_status:
            logger.error(e,exc_info=True)
        raise Exception('Could not execute sql script')
    finally:
        disconnect(conn,disconnect_flag)
    return exec_status
    
def is_valid(s):
    """
    Simple validity check of a variable for SQL tablename. Better to pass
    immutable variables as part of an execute command if possible
    """

    if s.startswith('pg_'): return False
    if not s.startswith(tuple(string.ascii_letters + '_')): return False
    if not re.match(r'^[\w\$]*',s): return False
    return True

def create_postgis(conn):
    """
    Tries to create postgis on the db connection. Returns the postgis full
    version information. If it fails it returns None
    """

    postgis_ver = None
    autocommit_old = bool(conn.autocommit)
    conn.autocommit = True
    cur = conn.cursor()
    sql = """CREATE EXTENSION postgis;"""
    cur.execute(sql)
    conn.autocommit = bool(autocommit_old)

    #confirm postgis install
    try:
        cur = conn.cursor()
        sql = """SELECT postgis_full_version();"""
        cur.execute(sql)
        postgis_ver = cur.fetchone()[0]
        if log_status:
            logger.info('postgis installed: {0}'.format(postgis_ver))
    except:
        if log_status:
            logger.error('postgis install could not be confirmed')

    return postgis_ver

def get_created_tables(schemas):
    """
    Parses the create table script to pull a list of the created tables
    """
    create_table = re.compile(r'((\s|^)create(\s)+?table(\s)+?(\w|\$)+?((\.)(\w|\$)+)*?(\s)*?(\())',flags=re.IGNORECASE)
    extract_table = re.compile(r'(?<=(table\s))(\w|\$)*((\.)(\w|\$)+)*?(?=((\s)*?)(\())',flags=re.IGNORECASE)
    dash_comment = re.compile(r'--(.)*(\n|\r|$)')
    slash_comment = re.compile(r'/\*(.|\n|\r)*?\*/')
    
    with open(schemas,'r') as sql_script:
        x = sql_script.read()
        y = re.sub(dash_comment,'',re.sub(slash_comment,'',x))
        script_tables = re.findall(create_table,y)
        extracted_tables = []
        for x in script_tables:
            #if log_status:
                #logger.debug(x[0])
            y = re.search(extract_table,x[0])
            #if log_status:
                #logger.debug(y.group(0))
            if y:
                extracted_tables.append(y.group(0))
        return extracted_tables

def insert_csv(path=None,conn=None,default_schema=None,default_name=None):
    """
    Try to insert the contents of all csvs in path into a database. Path can
    be a folder or a single file that you want to insert. Pass no path if you
    want to use the current working directory.
    
    Pass table_schema and table_name only if you wish to override the parsing 
    and force everything in a single schema and/or table. If only a table name 
    is specified the function attempts to insert into the default schema of 
    public. Input file name gets parsed for the schema and name of table unless
    values are specified (format schemaname_tablename.csv). If your table name has an underscore 
    make sure to pass table_schema.
    """
    
    tried_file = False
    n_added=0
    
    def fill_x(x):
        """
        Clean up the values read into a csv for use in the DB
        """
        y = x
        if x=='' or re.match(r'\[\]',x):
            y = None
        elif re.match(r'\[.*\]',x,re.DOTALL):
            try:
                y = eval('list('+x+')')
            except:
                pass
        return y

    try:
        #Get the list of files. If no path is specified use cwd
        if path==None:
            path = os.getcwd()
        if os.path.isdir(path):
            files = os.listdir(path)
        elif os.path.isfile(path):
            files = [os.path.basename(path),]
            path = os.path.dirname(path)
        else:
            raise Warning('Could not process input from path {0}'.format(path))
            
        
        #check if a database connection was passed
        if conn is None:
            raise Exception('Pass a database connection to insert CSVs')
      
        #Check if each is a csv and if so, try to insert it.
        for file in files:
            schema_table_name,file_ext = os.path.splitext(
                    os.path.basename(file))
            if check_ext(file_ext):
                tried_file=True
                if log_status:
                    logger.info('Reading {0}...'.format(os.path.basename(file)))
                table_schema,table_name=set_table_names(
                        schema_table_name,default_schema,default_name)
                file_name=os.path.normpath(os.path.join(path,file))
                #try to read the file
                with open(file_name,newline='',encoding='utf-8-sig') as r:
                    reader = csv.reader(r, delimiter=',',quotechar='"')
                    attr_names = next(reader)
                    n_attr = len(attr_names)
                    attr_names_csv = set(attr_names)
                    if len(attr_names_csv) != n_attr:
                        raise Exception('Duplicate column names in input file. Skipping the file.')
                                            
                    # check if the table exists
                    if not check_table_name(conn,table_schema,table_name):
                        raise Exception('Table {0} and/or schema {1} could not be found.'.format(
                                table_name,table_schema))
                    
                    # check if the fields in the csv match the fields in the table
                    cur = conn.cursor()
                    sql_check_attr = """SELECT column_name FROM information_schema.columns WHERE table_name = %s AND table_schema = %s;"""
                    cur.execute(sql_check_attr,(table_name,table_schema))
                    rows = cur.fetchall()
                    attr_names_db= set()
                    for row in rows:
                        attr_names_db.add(row[0])
                    cur.close()
                    if attr_names_csv != attr_names_db:
                        if attr_names_csv <= attr_names_db:
                            if log_status:
                                logger.warning('Input file contains a subset of table attributes. Trying to insert but may fail to insert if some fields are required. Check logs.')
                        else:
                            raise Exception('Input file has attributes that are not in the table.')
                    
                    #CSV Reading and inserting
                    #note at this point the table/schema and attribute names are validated against the db and are therefore safe to use without worrying about injection.
                    cur = conn.cursor()
                    attr_names_ins = ','.join(attr_names)
                    sql_insert = """INSERT INTO {0}.{1} ({2}) Values ({3});""".format(
                            table_schema,table_name,attr_names_ins, ','
                            .join(('%s',)*n_attr))
                    n_added_i = 0
                    error_rows = []
                    #if log_status:
                        #logger.debug(', '.join(row))
                    for row in reader:
                        try:
                            row = [fill_x(x) for x in row]
                            cur.execute(sql_insert,tuple(row))
                            conn.commit()
                            n_added_i += 1
                        except Exception as e:
                            if log_status:
                                logger.error(e)
                            conn.rollback()
                            error_rows.append(n_added_i + len(error_rows)+2)
                    cur.close()
                    if log_status:
                        logger.info('Added {0} rows to {1}.{2}.'
                          .format(n_added_i,table_schema,table_name))
                    if len(error_rows)>0:
                        if log_status:
                            logger.warning('Rows with errors (incl. header): ' + str(error_rows))
                n_added += n_added_i
        if not tried_file:
            raise Exception('No compatible files were provided.')
    except Warning as w:
        if log_status:
            logger.warning(w)
    except Exception as e:
        if log_status:
            logger.error(e,exc_info=True)
    finally:
        return n_added
        
def set_table_names(schema_table_name,table_schema=None,table_name=None):
    """
    Set table and schema names if not provided by user.
    """
    if table_schema==None and table_name==None:
        split_name = schema_table_name.split('_',1)
        if len(split_name)==1:
            table_schema = 'public'
            if log_status:
                logger.warning('Filename could not be split. Defaulting to public schema.')
        else:
            table_schema = split_name.pop(0)
        table_name = split_name[0]
    elif table_schema==None and table_name!=None:
        table_schema = 'public'
        if log_status:
            logger.info('Defaulting to public schema.')
    elif table_schema!=None and table_name==None:
        table_name = schema_table_name
    return (table_schema,table_name)
    if log_status:
        logger.debug('Checking file against table {0}.{1}'.format(
            table_schema,table_name))
        
def check_ext(file_ext=None):
    """
    Return true if the extension is in the allowed list
    """
    return (file_ext in __ext_types__)

def check_table_name(conn,table_schema,table_name):
    """
    Return true if the table exists in the connected database
    """
    cur = conn.cursor()
    sql_check_table = """SELECT EXISTS ( SELECT 1 
                        FROM information_schema.tables 
                        WHERE table_name = %s 
                        AND table_schema = %s);"""
    cur.execute(sql_check_table,(table_name,table_schema))
    db_exists = cur.fetchone()[0]
    cur.close()
    return db_exists

def disconnect(conn=None,db_connected=True):
    """
    Disconnect the Conn object if it exists, and the flag says True
    """
    if conn is not None and db_connected is True:
        conn.close()
        if log_status:
            logger.info('Database connection closed.')
        
def set_csv_size():
    """
    Increate the csv field size limit to accommodate country polygons 
    and other large data fields
    """
    max_int = sys.maxsize
    decrement = True
    x=csv.field_size_limit()
    if log_status:
        logger.debug('csv field size limit: {0}'.format(x))
    while decrement:
        # decrease the max_int value by factor 10 
        # as long as the OverflowError occurs.
        
        decrement = False
        try:
            csv.field_size_limit(max_int)
        except OverflowError:
            max_int = int(max_int/10)
            decrement = True
    if x!=csv.field_size_limit():
        if log_status:
            logger.debug('new csv field size limit: {0}'.format(csv.field_size_limit()))
        
def prep_int(df,cols):
        """
        Hack job to get around pandas refusing to store nans, and python not knowing
        how to explicitly store a float without scientific notation.
        
        Fill in with a placehodler value that is not in the data set (fill_val). Then recast everything to strings. Then drop the fill_val entries to nan.
        Now you have a column that is df type object with strings in it, and nan values where they should be.
        
        This won't work if every possible negative 64-bit integer value is being used in the column.
        
        cols is either a string for a single column, or a list of column names.
        df is the dataframe (obv)
        """
        int_type = 'int64'
        obj_type = 'object'
        if type(cols)!=list:
            cols = [cols]
        for col in cols:
            if df[col].dtype not in ['int32','int64']: #this function can be skipped if it's already an integer type column in pandas
                fill_val = -1
                while fill_val in set(df[col].dropna().tolist()) and fill_val>-sys.maxsize:
                    fill_val = fill_val - 1
                df[col] = df[col].fillna(fill_val).astype(int_type)
                df[col] = df[col].astype(obj_type).apply(lambda x: str(x))
                df[col].replace('{0}'.format(fill_val),pd.np.nan,inplace=True)
        return df

#set max int for csv reading to a large value the system can still handle
set_csv_size()



if __name__== '__main__':
    init()