import psycopg2
import dbproto
import os

config_file = 'dbproto/tests/test_dbconfig.yml'
__env_name__ = os.getenv('TEST_TYPE', 'local_test')
__env_name__ = __env_name__ + '_cli'
os.system("python bin/build_db {0} -et local -cf {1}".format(__env_name__,config_file))

dbproto.yml_config(config_file)
env_params = dbproto.get_env(__env_name__)


def run_query(sql):
    conn = None
    try:
        conn = psycopg2.connect(**env_params.database.getDict())
        cur = conn.cursor()
        cur.execute(sql)
        conn.commit()
        try:
            x = cur.fetchall()
        except:
            x = None
    finally:
        if conn:
            dbproto.disconnect(conn)
    if len(x)==1 and x is not None:
        x = x[0] #unpack x if its only one row
    return x

def test_verify_postgres_cli():
    sql = """SELECT version();"""
    sql_ver = run_query(sql)[0]
    assert sql_ver is not None
    
def test_verify_postgis_cli():
    '''Test if postgis was installed'''
    global env_params
    sql = """SELECT postgis_full_version();"""
    postgis_ver = [True if env_params.postgis == False else run_query(sql)[0]]
    assert postgis_ver is not None

def test_create_players_schema_cli():
    '''Test if the schema was created'''
    sql = """SELECT schema_name FROM information_schema.schemata WHERE schema_name = 'players';"""
    try:
        schema_created = run_query(sql)[0]
    except:
        schema_created = None
    assert schema_created == 'players'

def test_create_players_info_table_cli():
    '''Test if the table was created'''
    sql = """SELECT table_name FROM information_schema.tables WHERE table_schema = 'players' AND table_name='info';"""
    try:
        table_created = run_query(sql)[0]
    except:
        table_created = None
    assert table_created == 'info'

def test_db_insert_cli():
    '''Test if data were entered into the players schema, info table'''
    sql = """SELECT COUNT(*) FROM players.info;"""
    try:
        n_items = run_query(sql)[0]
    except:
        n_items = None
    assert n_items > 0