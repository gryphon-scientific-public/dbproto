CREATE SCHEMA players;

CREATE TABLE players.info (
    --List of players
    name text NOT NULL,
    pos text NOT NULL,
    hof boolean NOT NULL,
    CONSTRAINT players_pkey PRIMARY KEY (name)
);