dbproto
---------

Simple, extensible database prototyping tools.

To install:

```pip install .```

To configure yaml files and data to build a database, follow the examples in the dbproto/tests folder.

dbproto includes a test suite. To run tests type 'python setup.py test'. Tests for the BSVE will only work if you have a BSVE api token set up.

Databases can be built directly from the command line. To use command line features, run the build_db script in the bin folder ('python bin/build_db -h'). Examples using the command line to build the database are in the dbproto/tests folder.

dbproto is released under the MIT license (see LICENSE file).